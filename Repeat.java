/**
*
* class Repeat
*
* generate recursive sequence
*
* rule a if number n is even divide by 2
*
* rule b if number n is odd do 3*n+1
*
* ----- compile -----
*
* javac Repeat.java
*
* ----- run example -----
*
* java Repeat 3
*
*/
class Repeat{
    public static void main(String []args){
        int item;
        item=Integer.parseInt(args[0]);
        for(;(item!=1) && (item!=-1);){
            System.out.println(item);
            if((item%2) == 0){
                item=item/2;
            }else{
                item=3*item+1;
            }
        }
        System.out.println(item);
    }
}
